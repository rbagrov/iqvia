import os
import unittest

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from celery import Celery
from celery.schedules import crontab
from celery.utils.log import get_task_logger
from datetime import timedelta

from app import blueprint
from app.main import create_app, db
from app.main.model.models import *  # noqa
from tasks import *  # noqa

logger = get_task_logger(__name__)

app = create_app(os.getenv('BOILERPLATE_ENV') or 'dev')
app.register_blueprint(blueprint)

app.app_context().push()

manager = Manager(app)

migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)


# Celery
app.config['CELERYBEAT_SCHEDULE'] = {
    'input_random-every-minute': {
        'task': 'input_random',
        'schedule': timedelta(seconds=15)
    },
    'remove_oldies-every-minute': {
        'task': 'remove_oldies',
        'schedule': crontab(minute="*")
    }
}

celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


class ContextTask(celery.Task):
    abstract = True

    def __call__(self, *args, **kwargs):
        with app.app_context():
            return celery.Task.__call__(self, *args, **kwargs)


celery.Task = ContextTask


@manager.command
def run():
    '''
    Run server
    '''
    app.run()


@manager.command
def test():
    '''
    Run tests
    '''
    tests = unittest.TestLoader().discover('app/test', pattern='test*.py')
    tests_result = unittest.TextTestRunner(verbosity=1).run(tests)
    if tests_result.wasSuccessful():
        return 0
    return 1


if __name__ == '__main__':
    manager.run()
