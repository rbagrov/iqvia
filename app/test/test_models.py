import unittest
from faker import Faker

from app.main import db
from app.main.model.models import Contact, Email
from app.test.base import BaseTestCase


faker = Faker()


class TestUserModel(BaseTestCase):

    def test_add_contact(self):
        email = Email(email=faker.email())
        contact = Contact(
                username=faker.word(),
                firstname=faker.first_name(),
                lastname=faker.last_name())
        contact.emails.append(email)
        db.session.add(contact)
        db.session.commit()

        self.assertEqual(1, db.session.query(Contact).count())

    def test_remove_contact(self):
        email = Email(email=faker.email())
        contact = Contact(
                username=faker.word(),
                firstname=faker.first_name(),
                lastname=faker.last_name())
        contact.emails.append(email)
        db.session.add(contact)
        db.session.commit()

        db.session.delete(contact)
        db.session.commit()

        self.assertEqual(0, db.session.query(Contact).count())

    def test_update_contact(self):
        email = Email(email=faker.email())
        username = faker.word()
        contact = Contact(
                username=username,
                firstname=faker.first_name(),
                lastname=faker.last_name())
        contact.emails.append(email)
        db.session.add(contact)
        db.session.commit()

        new_firstname = faker.first_name()
        contact.firstname = new_firstname
        db.session.commit()

        updated = Contact.query.filter_by(username=username).first()
        self.assertEqual(new_firstname, updated.firstname)


if __name__ == '__main__':
    unittest.main()
