import unittest
import json
from faker import Faker

from app.main import db
from app.main.model.models import Contact
from app.test.base import BaseTestCase


faker = Faker()


class TestAPI(BaseTestCase):

    def test_api_create_contact(self):
        response = self.client.post(
                '/contact/',
                data=json.dumps(dict(
                    username=faker.word(),
                    firstname=faker.first_name(),
                    lastname=faker.last_name(),
                    email=faker.email()
                )),
                content_type='application/json'
        )
        self.assertEqual(201, response.status_code)
        self.assertEqual(1, db.session.query(Contact).count())

    def test_api_delete_contact(self):
        username = faker.word()
        self.client.post(
            '/contact/',
            data=json.dumps(dict(
                username=username,
                firstname=faker.first_name(),
                lastname=faker.last_name(),
                email=faker.email()
            )),
            content_type='application/json'
        )

        response = self.client.delete('/contact/{}'.format(username))

        self.assertEqual(200, response.status_code)
        self.assertEqual(0, db.session.query(Contact).count())

    def test_api_update_contact(self):
        username = faker.word()
        firstname = faker.first_name()
        lastname = faker.last_name()
        email = faker.email()

        self.client.post(
            '/contact/',
            data=json.dumps(dict(
                username=username,
                firstname=firstname,
                lastname=lastname,
                email=email
            )),
            content_type='application/json'
        )

        new_firstname = faker.first_name()
        response = self.client.put(
            '/contact/{}'.format(username),
            data=json.dumps(dict(
                username=username,
                firstname=new_firstname,
                lastname=lastname
            )),
            content_type='application/json'
        )

        updated = Contact.query.filter_by(username=username).first()
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_firstname, updated.firstname)

    def test_get_single_contact(self):
        username = faker.word()
        self.client.post(
            '/contact/',
            data=json.dumps(dict(
                username=username,
                firstname=faker.first_name(),
                lastname=faker.last_name(),
                email=faker.email()
            )),
            content_type='application/json'
        )

        response = self.client.get('/contact/{}'.format(username))

        self.assertEqual(200, response.status_code)

        data = json.loads(response.data.decode())
        self.assertTrue(data['username'] == username)

    def test_get_contact_list(self):
        username1 = faker.word()
        self.client.post(
            '/contact/',
            data=json.dumps(dict(
                username=username1,
                firstname=faker.first_name(),
                lastname=faker.last_name(),
                email=faker.email()
            )),
            content_type='application/json'
        )

        username2 = faker.word()
        self.client.post(
            '/contact/',
            data=json.dumps(dict(
                username=username2,
                firstname=faker.first_name(),
                lastname=faker.last_name(),
                email=faker.email()
            )),
            content_type='application/json'
        )

        response = self.client.get('/contact/')

        self.assertEqual(200, response.status_code)

        data = json.loads(response.data.decode())
        self.assertTrue(data['data'][0]['username'] == username1)
        self.assertTrue(data['data'][1]['username'] == username2)


if __name__ == '__main__':
    unittest.main()
