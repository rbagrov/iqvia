from flask_restplus import Namespace, fields


class ContactDto():
    api = Namespace('contact', description='contact related operations')
    email = api.model('email', {
        'email': fields.String(required=False, description='email')})
    contact = api.model('contact', {
        'username': fields.String(required=True, description='username'),
        'firstname': fields.String(required=True, description='firstname'),
        'lastname': fields.String(required=True, description='lastname'),
        'emails': fields.List(fields.Nested(email)),
    })
