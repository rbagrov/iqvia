from flask import request
from flask_restplus import Resource

from ..util.dto import ContactDto
from ..service.contact_service import (save_new_contact,
                                       get_all_contacts,
                                       get_a_contact,
                                       update_contact,
                                       delete_contact,
                                       )

api = ContactDto.api
_contact = ContactDto.contact


@api.route('/')
class ContactList(Resource):
    @api.doc('list_of_all_contacts')
    @api.marshal_list_with(_contact, envelope='data')
    def get(self):
        """List all contacts"""
        return get_all_contacts()

    @api.expect(_contact, validate=True)
    @api.response(201, 'Contact successfully created.')
    @api.doc('create new contact')
    def post(self):
        """Creates contact """
        data = request.json
        return save_new_contact(data=data)


@api.route('/<username>')
@api.param('username', 'The Contact identifier')
@api.response(404, 'Contact not found.')
class CreateContact(Resource):
    @api.doc('get contact')
    @api.marshal_with(_contact)
    def get(self, username):
        """get contact given its username"""
        contact = get_a_contact(username)
        if not contact:
            api.abort(404)
        else:
            return contact

    @api.expect(_contact, validate=True)
    @api.response(200, 'Contact successfully updated.')
    @api.doc('update new contact')
    def put(self, username):
        """Updates contact """
        data = request.json
        return update_contact(username=username, data=data)

    @api.doc('delete contact')
    def delete(self, username):
        """delete contact given its username"""
        return delete_contact(username)
