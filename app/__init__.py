from flask_restplus import Api
from flask import Blueprint

from .main.controller.contact_controller import api as contact_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='IQVIA RESTPLUS API',
          version='1.0',
          description='flask restplus service'
          )

api.add_namespace(contact_ns, path='/contact')
