from app.main import db
from app.main.model.models import Contact, Email


def save_new_contact(data):
    contact = Contact.query.filter_by(username=data['username']).first()
    if not contact:
        new_contact = Contact(
            username=data['username'],
            firstname=data.get('firstname'),
            lastname=data.get('lastname'),
        )
        if data.get('email'):
            email = Email(email=data.get('email'))
            new_contact.emails.append(email)
        add(new_contact)
        response_object = {
            'status': 'success',
            'message': 'Successfully created.',
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Contact already exists.',
        }
        return response_object, 409


def update_contact(username, data):
    contact = Contact.query.filter_by(username=username).first()
    if contact:
        contact.username = data['username']
        contact.firstname = data['firstname']
        contact.lastname = data['lastname']

        if data.get('email'):
            email = Email(email=data.get('email'))
            contact.emails.append(email)

        update()
        response_object = {
            'status': 'success',
            'message': 'Successfully updated.',
        }
        return response_object, 200
    else:
        response_object = {
            'status': 'fail',
            'message': 'Contact do not exists.',
        }
        return response_object, 409


def get_all_contacts():
    return Contact.query.all()


def get_a_contact(username):
    return Contact.query.filter_by(username=username).first()


def delete_contact(username):
    contact = Contact.query.filter_by(username=username).first()
    delete(contact)
    response_object = {'status': 'ok'}
    return response_object, 200


def add(data):
    db.session.add(data)
    db.session.commit()


def delete(data):
    db.session.delete(data)
    db.session.commit()


def update():
    db.session.commit()
