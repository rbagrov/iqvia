from sqlalchemy.orm import relationship
from .. import db


class Contact(db.Model):
    """ Contact Model"""
    __tablename__ = "contact"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_on = db.Column(db.DateTime, server_default=db.func.now())
    username = db.Column(db.String(30), unique=True)
    firstname = db.Column(db.String(80), unique=False)
    lastname = db.Column(db.String(80), unique=False)
    emails = relationship('Email', cascade="all,delete")

    def __repr__(self):
        return "Contact: {}".format(self.username)


class Email(db.Model):
    """ Email Model """
    __tablename__ = "email"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))
    email = db.Column(db.String(120), unique=True)

    def __repr__(self):
        return "Email: {}".format(self.email)
