import celery

from app.main import db
from app.main.model.models import Contact, Email


@celery.task(name='input_random')
def input_random():
    logger = input_random.get_logger()
    from faker import Faker
    faker = Faker()

    email = Email(email=faker.email())
    new_contact = Contact(
            username=faker.word(),
            firstname=faker.first_name(),
            lastname=faker.last_name())
    new_contact.emails.append(email)
    db.session.add(new_contact)
    db.session.commit()
    logger.info("Username {} created!".format(new_contact.id))


@celery.task(name='remove_oldies')
def remove_oldies():
    from datetime import datetime, timedelta
    logger = input_random.get_logger()

    date = (datetime.utcnow() - timedelta(minutes=1)).date()
    contacts = Contact.query.filter(db.func.date(Contact.created_on) <= date).all()
    for contact in contacts:
        db.session.delete(contact)
        logger.info("Username {} deleted!".format(contact.id))
    db.session.commit()
